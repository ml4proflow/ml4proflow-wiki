# ml4proflow-wiki

## What is ml4proflow?
ml4proflow is a lightweight, modular and scalable framework to manage the dataflow along a pipeline.

The framework is divided into independent installable subprojects. These are packaged in individual Python modules so that they can be installed and managed using package management systems such as [pip](https://pypi.org/project/pip/). 
A short overview of the framework structure is given in the image below.

![ml4proflow: The structure of the framework](img/framework_structure.png)

The core of the framework is located in the repository [ml4proflow](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow), which defines the interfaces of the modules. To transfer data from one to another module, the framework passes [_Pandas Dataframes_](https://pandas.pydata.org/docs/index.html) via _push()_ and _pull()_ methods to achieve a many-to-many communication. All algorithms, used in individual pipelines are packaged into modules using the interface implemented in the core framework. Modules are classified with the prefix _ml4proflow-mods-_. 

Typically modules have one to many inputs and one to many outputs. However, there are exceptions, such as the [IO-Modules](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-io), which are used at the beginning resp. at the end of a pipeline. To achieve high variability and to use all modules independent from each other, modules can be adapt with a configuration, to set specific parameters for the modules functionality.
An overwiev of the module structure is given below. 

![ml4proflow: The structure of a module](img/module_structure)

Repositories with the prefix _ml4proflow-data-_ are providing some experimental data to reproduce specific processes.
Tools for analyzing various aspects (e.g. performance) of individual modules or entire pipelines are located in repositories with the prefix _ml4proflow-eval-_. 

Entire dataflow pipelines are formed by concatenating multiple modules and desccribing their configurations are called _Data processing graphs_. These graphs can thus be individually tailored to each application. 

Since we want to enable many different user groups with different previous knowledge to use _ml4proflow_, there are different ways and possibilities to use our framework. Therefore, core algorithms and user interfaces are separate from each other. _ml4proflow_ provides a user interface as [Jupyter Notebook](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-jupyter) with a configurable [start screen](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-jupyter-launcher) and the integration in [Node-RED](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-nodered). Of course it is also possible to interact with framework with a command-line interface.


## How To's
This wiki provides illustrated installation instructions. For more information, see the instructions for [the Standalone environment](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-wiki/-/blob/main/Installationsanleitung-Standalone.pdf) resp. the [Docker environment](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-wiki/-/blob/main/Installationsanleitung-Docker.pdf) or the corresponding repoistories ([ml4proflow-standalone](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-standalone) or [ml4proflow-docker](https://gitlab.ub.uni-bielefeld.de/ml4proflow/docker)).
> Note: The installation instructions are written in german


## Naming convention 
Since the framework supports multiple backends as well as frontends and also the requirements vary from developer to user, we decide to pre-configure several versions of the framework.
The naming convention to identificate the desired version is listed below.

| backend | frontend | set | version | available |
| ------ | ------ | ------ | ------ | ------ |
| docker | all | all | "" | yes |
| docker | all | all | dev | yes |
| docker | cli | minimal | dev | no |
| docker | jupyter | minimal | dev | yes |
| docker | noderedjupyter | minimal | dev | yes |
| env-install | cli | minimal | dev | yes |
| env-install | jupyter | all | "" | yes | 
